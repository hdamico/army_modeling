class CreateCivilizations < ActiveRecord::Migration[5.2]
  def change
    create_table :civilizations do |t|
      t.string :name, null: false
      t.jsonb :default_units, default: {}

      t.timestamps
    end
  end
end
